import commentRepository from '../../data/repositories/commentRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const updateById = (id, comment) => commentRepository.updateById(id, comment);

export const getCommentById = id => commentRepository.getCommentById(id);

export const deleteById = async id => {
  const result = await commentRepository.deleteById(id, false);
  if (!result) {
    throw new Error('Comment wasn\'t deleted');
  }
  return {};
};

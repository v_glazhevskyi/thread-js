const isNotEmpty = obj => obj && Object.keys(obj).length;
const print = (label, obj) => isNotEmpty(obj) && console.log(label, obj);

export default (req, res, next) => {
  const { method, path, params, body } = req;
  console.log(`${method} ${path}`);
  print('params:', params);
  print('body:', body);
  next();
};

import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body) // should be before .put(/:id, ...)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) react his post
        if (req.body.isLike === true) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        }
        if (req.body.isLike === false) {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
        }
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.updateById(req.params.id, req.body)
    .then(post => {
      req.io.emit('update_post', post); // notify all users that a post was updated
      return res.send(post);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deleteById(req.params.id)
    .then(result => {
      req.io.emit('delete_post'); // notify all users that a post was deleted
      return res.send(result);
    })
    .catch(next));

export default router;

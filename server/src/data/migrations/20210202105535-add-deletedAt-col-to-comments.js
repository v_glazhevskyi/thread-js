module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'comments',
    'deletedAt',
    { type: Sequelize.DATE }
  ),

  down: queryInterface => queryInterface.removeColumn(
    'comments',
    'deletedAt'
  )
};

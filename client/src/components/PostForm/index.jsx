import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Message } from 'semantic-ui-react';

import styles from './styles.module.scss';

const PostForm = ({
  onSubmit: submit,
  uploadImage,
  post
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(post.image);
  const [isUploading, setIsUploading] = useState(false);
  const [error, setError] = useState(null);

  const handleSubmit = async () => {
    if (!body) {
      return;
    }
    const imageId = image ? image.id : null;
    await submit({ ...post, image, imageId, body });
    setBody('');
    setImage(null);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id, link } = await uploadImage(target.files[0]);
      setImage({ id, link });
    } catch (err) {
      setError(err);
      console.error(err);
    } finally {
      setIsUploading(false);
    }
  };

  const removeImage = () => setImage(null);

  const handleDismiss = () => setError(null);

  return (
    <Form onSubmit={handleSubmit}>
      <Form.TextArea
        name="body"
        value={body}
        placeholder="What is the news?"
        onChange={ev => setBody(ev.target.value)}
      />
      {image?.link && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image.link} alt="post" />
        </div>
      )}
      <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
        <Icon name="image" />
        {image ? 'Replace image' : 'Attach image'}
        <input name="image" type="file" onChange={handleUploadFile} hidden />
      </Button>

      {image && (
        <Button color="red" type="button" onClick={removeImage}>
          <Icon name="remove" />
          Remove image
        </Button>
      )}

      <Button floated="right" color="blue" type="submit">Save</Button>

      {error && <Message negative onDismiss={handleDismiss} content={error.message} />}
    </Form>
  );
};

PostForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  post: PropTypes.objectOf(PropTypes.any)
};

PostForm.defaultProps = {
  post: { body: '', image: null, imageId: null }
};

export default PostForm;

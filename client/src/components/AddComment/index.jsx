import React from 'react';
import PropTypes from 'prop-types';
import CommentForm from 'src/components/CommentForm';

const AddComment = ({
  postId,
  addComment
}) => (
  <CommentForm onSubmit={comment => addComment({ ...comment, postId })} />
);

AddComment.propTypes = {
  addComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export default AddComment;

import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'semantic-ui-react';

const ConfirmDialog = ({
  title,
  message,
  open,
  onConfirm,
  onClose
}) => (
  <Modal size="tiny" open={open} onClose={onClose}>
    <Modal.Header>{title}</Modal.Header>
    <Modal.Content>
      <p>{message}</p>
    </Modal.Content>
    <Modal.Actions>
      <Button negative onClick={onConfirm}>
        Yes
      </Button>
      <Button positive onClick={onClose}>
        No
      </Button>
    </Modal.Actions>
  </Modal>
);

ConfirmDialog.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  open: PropTypes.bool,
  onConfirm: PropTypes.func,
  onClose: PropTypes.func
};

ConfirmDialog.defaultProps = {
  title: 'Default title',
  message: 'Default message',
  open: true,
  onConfirm: () => console.log('Default "onConfirm" handler'),
  onClose: () => console.log('Default "onClose" handler')
};

export default ConfirmDialog;

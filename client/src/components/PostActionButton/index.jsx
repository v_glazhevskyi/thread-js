import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';

const PostActionButton = ({ children, ...restProps }) => (
  <Button {...restProps} size="mini" compact floated="right" type="button">
    {children}
  </Button>
);

PostActionButton.propTypes = {
  children: PropTypes.string.isRequired
};

export default PostActionButton;

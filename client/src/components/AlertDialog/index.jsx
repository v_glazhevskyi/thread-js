import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, Message } from 'semantic-ui-react';

const AlertDialog = ({
  title,
  message,
  open,
  onClose
}) => (
  <Modal size="mini" open={open} onClose={onClose}>
    <Message>
      {title && <Message.Header>{title}</Message.Header>}
      <p>{message}</p>
      <Button color="blue" onClick={onClose}>
        OK
      </Button>
    </Message>
  </Modal>
);

AlertDialog.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func
};

AlertDialog.defaultProps = {
  title: 'Default title',
  message: 'Default message',
  open: true,
  onClose: () => console.log('Default "onClose" handler')
};

export default AlertDialog;

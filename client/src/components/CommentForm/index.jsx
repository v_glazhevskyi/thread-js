import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon } from 'semantic-ui-react';

const CommentForm = ({
  onSubmit: submit,
  comment
}) => {
  const [body, setBody] = useState(comment.body);

  const handleSubmit = async () => {
    if (!body) {
      return;
    }
    await submit({ ...comment, body });
    setBody('');
  };

  return (
    <Form reply onSubmit={handleSubmit}>
      <Form.TextArea
        name="body"
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" labelPosition="left" icon primary>
        <Icon name="edit" />
        {comment.body ? 'Update' : 'Post comment'}
      </Button>
    </Form>
  );
};

CommentForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  comment: PropTypes.objectOf(PropTypes.any)
};

CommentForm.defaultProps = {
  comment: { body: '' }
};

export default CommentForm;

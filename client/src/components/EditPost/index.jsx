import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header } from 'semantic-ui-react';
import PostForm from 'src/components/PostForm';

const EditPost = ({
  post,
  updatePost,
  uploadImage,
  onClose
}) => (
  <Modal dimmer="blurring" centered={false} open={Boolean(post)} onClose={onClose}>
    <Modal.Header>
      <Header>Edit Post</Header>
    </Modal.Header>
    <Modal.Content>
      <PostForm
        onSubmit={updatePost}
        uploadImage={uploadImage}
        post={post}
      />
    </Modal.Content>
  </Modal>
);

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any),
  updatePost: PropTypes.func,
  uploadImage: PropTypes.func,
  onClose: PropTypes.func
};

EditPost.defaultProps = {
  post: null,
  updatePost: data => console.log('Default "updatePost" handler ->', data),
  uploadImage: data => console.log('Default "uploadImage" handler ->', data),
  onClose: () => console.log('Default "onClose" handler')
};

export default EditPost;

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body, createdAt, user },
  isOwn,
  editComment,
  deleteComment
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      {isOwn && (
        <CommentUI.Actions>
          <CommentUI.Action onClick={editComment}>
            Edit
          </CommentUI.Action>
          <CommentUI.Action onClick={deleteComment}>
            Remove
          </CommentUI.Action>
        </CommentUI.Actions>
      )}
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  isOwn: PropTypes.bool,
  editComment: PropTypes.func,
  deleteComment: PropTypes.func
};

Comment.defaultProps = {
  isOwn: false,
  editComment: () => console.log('Default "editComment" handler'),
  deleteComment: () => console.log('Default "deleteComment" handler')
};

export default Comment;

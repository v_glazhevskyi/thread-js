import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header } from 'semantic-ui-react';
import CommentForm from 'src/components/CommentForm';

const EditComment = ({
  comment,
  updateComment,
  open,
  onClose
}) => (
  <Modal dimmer="blurring" centered={false} size="small" open={open} onClose={onClose}>
    <Modal.Header>
      <Header>Edit Comment</Header>
    </Modal.Header>
    <Modal.Content>
      <CommentForm
        onSubmit={updateComment}
        comment={comment}
      />
    </Modal.Content>
  </Modal>
);

EditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any),
  updateComment: PropTypes.func,
  open: PropTypes.bool,
  onClose: PropTypes.func
};

EditComment.defaultProps = {
  comment: null,
  updateComment: data => console.log('Default "updateComment" handler ->', data),
  open: true,
  onClose: () => console.log('Default "onClose" handler')
};

export default EditComment;

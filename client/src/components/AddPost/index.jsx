import React from 'react';
import PropTypes from 'prop-types';
import { Segment } from 'semantic-ui-react';
import PostForm from 'src/components/PostForm';

const AddPost = ({
  addPost,
  uploadImage
}) => (
  <Segment>
    <PostForm
      onSubmit={addPost}
      uploadImage={uploadImage}
    />
  </Segment>
);

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;

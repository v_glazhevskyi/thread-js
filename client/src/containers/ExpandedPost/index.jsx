import React, { useReducer } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import EditComment from 'src/components/EditComment';
import Spinner from 'src/components/Spinner';
import ConfirmDialog from 'src/components/ConfirmDialog';
import AlertDialog from 'src/components/AlertDialog';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment
} from 'src/containers/Thread/actions';

const initialState = {
  selectedComment: null,
  dialog: {
    name: '',
    title: 'Default title',
    message: 'Default message'
  }
};

const uiReducer = (state, action) => {
  switch (action.type) {
    case 'CONFIRM_DELETE':
      return {
        ...state,
        selectedComment: action.comment,
        dialog: {
          name: 'CONFIRM_DELETE',
          title: 'Remove comment',
          message: 'Are you sure?'
        }
      };
    case 'EDIT_COMMENT':
      return {
        ...state,
        selectedComment: action.comment,
        dialog: {
          name: 'EDIT_COMMENT'
        }
      };
    case 'RESET':
      return initialState;
    case 'ERROR':
      return {
        ...state,
        dialog: {
          name: 'ERROR_ALERT',
          title: 'Error',
          message: action.message
        }
      };
    default:
      return state;
  }
};

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  updateComment: update,
  deleteComment: remove
}) => {
  const [state, dispatch] = useReducer(uiReducer, initialState);

  const closeDialog = () => dispatch({ type: 'RESET' });
  const showError = message => dispatch({ type: 'ERROR', message });
  const confirmDelete = comment => () => dispatch({ type: 'CONFIRM_DELETE', comment });
  const setCommentToEdit = comment => () => dispatch({ type: 'EDIT_COMMENT', comment });

  const deleteCommentHandler = async () => {
    try {
      await remove(state.selectedComment);
      closeDialog();
    } catch (error) {
      console.error(error);
      showError('Comment wasn\'t deleted');
    }
  };

  const updateCommentHandler = async updatedComment => {
    try {
      await update(updatedComment);
      closeDialog();
    } catch (error) {
      showError('Comment wasn\'t updated');
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    isOwn={userId === comment.userId}
                    deleteComment={confirmDelete(comment)}
                    editComment={setCommentToEdit(comment)}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
            <EditComment
              comment={state.selectedComment}
              updateComment={updateCommentHandler}
              open={state.dialog.name === 'EDIT_COMMENT'}
              onClose={closeDialog}
            />
            <ConfirmDialog
              title={state.dialog.title}
              message={state.dialog.message}
              open={state.dialog.name === 'CONFIRM_DELETE'}
              onConfirm={deleteCommentHandler}
              onClose={closeDialog}
            />
            <AlertDialog
              title={state.dialog.title}
              message={state.dialog.message}
              open={state.dialog.name === 'ERROR_ALERT'}
              onClose={closeDialog}
            />
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  userId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);

import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = post => async (dispatch, getRootState) => {
  const { body, imageId } = post;
  const { posts: { posts } } = getRootState();
  const { id } = await postService.updatePost(post.id, { body, imageId });
  const updatedPost = await postService.getPost(id);
  const updated = posts.map(item => (item.id === id ? updatedPost : item));
  dispatch(setPostsAction(updated));
};

export const deletePost = post => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  await postService.deletePost(post.id);
  const updated = posts.filter(item => item.id !== post.id);
  dispatch(setPostsAction(updated));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const reactToPost = (postId, { isLike = true }) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.setPostReaction(postId, { isLike });
  const diff = id ? 1 : -1; // if ID exists then the post reaction was created/updated, otherwise - reaction was removed

  // If timestams different, then post reaction was changed to opposite
  // Diffs is taken from the current closure
  const calc = (count, reaction) => (
    createdAt !== updatedAt
      ? Number(count) + (reaction ? diff : -diff)
      : Number(count) + (reaction ? diff : 0)
  );

  const updateReactions = post => {
    if (post.id !== postId) {
      return post;
    }

    return {
      ...post,
      likeCount: calc(post.likeCount, isLike),
      dislikeCount: calc(post.dislikeCount, !isLike)
    };
  };

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(updateReactions);

  dispatch(setPostsAction(updated));

  if (expandedPost) {
    dispatch(setExpandedPostAction(updateReactions(expandedPost)));
  }
};

export const likePost = postId => reactToPost(postId, { isLike: true });
export const dislikePost = postId => reactToPost(postId, { isLike: false });

const updateCommentsState = (comment, mapComments, dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const addComment = newComment => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(newComment);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  updateCommentsState(comment, mapComments, dispatch, getRootState);
};

export const updateComment = updatedComment => async (dispatch, getRootState) => {
  const { id: commentId, body } = updatedComment;
  const { id } = await commentService.updateComment(commentId, { body });
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || [])]
      .map(item => (
        item.id === comment.id
          ? comment
          : item
      )) // comment is taken from the current closure
  });

  updateCommentsState(comment, mapComments, dispatch, getRootState);
};

export const deleteComment = comment => async (dispatch, getRootState) => {
  await commentService.deleteComment(comment.id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...(post.comments || [])]
      .filter(item => item.id !== comment.id) // comment is taken from the current closure
  });

  updateCommentsState(comment, mapComments, dispatch, getRootState);
};
